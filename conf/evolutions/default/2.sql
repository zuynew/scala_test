# Stores schema

# --- !Ups

CREATE TABLE tbl_store
(
    id BIGSERIAL PRIMARY KEY,
    name varchar(100) NOT NULL UNIQUE
);

# --- !Downs
