package services

import dao.UserDAO
import exceptions.user.UserExistsException
import models.User
import org.mindrot.jbcrypt.BCrypt

import scala.concurrent.{ExecutionContext, Future}

class UserService(userDAO: UserDAO)(implicit ec: ExecutionContext) {

    def create(login: String, password: String): Future[Either[UserExistsException,User]] = {
      userDAO.findByLogin(login).flatMap({
        case None => userDAO.insert(User(login = login, password = BCrypt.hashpw(password, BCrypt.gensalt()))).map(Right(_))
        case _ => Future(Left(new UserExistsException))
      })
    }

}
