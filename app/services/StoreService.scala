package services

import dao.StoreDAO
import exceptions.store.StoreNotFoundException
import models.StoreWithProducts

import scala.concurrent.{ExecutionContext, Future}

class StoreService(storeDAO: StoreDAO)(implicit ec: ExecutionContext) {

  def getByIdWithProducts(storeId: Int): Future[Either[StoreNotFoundException, StoreWithProducts]] = {
    storeDAO.findByIdWithProducts(storeId).map({
      case None => Left(StoreNotFoundException())
      case Some(store) => Right(store)
    })
  }

}
