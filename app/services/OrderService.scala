package services

import dao.{OrderDAO, OrderItemDAO}
import exceptions.order.OpenOrderExistsException
import models.{Order, OrderItem, User}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}

class OrderService(
  db: Database,
  orderDAO: OrderDAO,
  orderItemDAO: OrderItemDAO
)(implicit ec: ExecutionContext) {

  def create(user: User): Future[Either[Throwable, Order]] = {

    user.id match {
      case None => Future(Left(new IllegalArgumentException("User should have id")))
      case Some(id) => orderDAO.findOpenOrderByUserId(id).flatMap({
        case None => orderDAO.insert(Order(userId = id, status = "open")).map(Right(_))
        case Some(_) => Future(Left(OpenOrderExistsException()))
      })
    }

  }

  def addItem(order: Order, product: models.Product, quantity: Int) = {
    orderItemDAO.insert(OrderItem(orderId = order.id.get, productId = product.id.get, quantity = quantity))
  }

}
