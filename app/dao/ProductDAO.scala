package dao

import models.ProductTable

import scala.concurrent.{Future, ExecutionContext}
import slick.driver.PostgresDriver.api._

class ProductDAO(db: Database)(implicit ec: ExecutionContext)  {

  private val products = TableQuery[ProductTable]

  def findByStoreId(storeId: Int): Future[Seq[models.Product]] = db.run(products.filter(_.storeId === storeId).result)

  def findById(productId: Int): Future[Option[models.Product]] = db.run(products.filter(_.id === productId).result.headOption)

  def all(): Future[Seq[models.Product]] = db.run(products.result)

  def insert(product: models.Product): Future[models.Product] =  db.run( ( ( products returning products.map(_.getFields) ) += product ).map(fields => (models.Product.apply _).tupled(fields)) )

}
