package dao

import models.{StoreWithProducts, ProductTable, Store, StoreTable}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}

class StoreDAO(db: Database, productDAO: ProductDAO)(implicit ec: ExecutionContext)  {

  private val stores = TableQuery[StoreTable]

  def all(): Future[Seq[Store]] = db.run(stores.result)

  def findById(storeId: Int): Future[Option[Store]] = db.run( stores.filter(_.id === storeId).take(1).result ).map(_.headOption)

  def findByIdWithProducts(storeId: Int): Future[Option[StoreWithProducts]] = {
    findById(storeId).flatMap({
      case None => Future(None)
      case Some(store) => productDAO.findByStoreId(store.id.get).map(productsSeq => Some(StoreWithProducts(store, productsSeq)))
    })
  }

  def findByName(name: String): Future[Option[Store]] = db.run( stores.filter(_.name === name).take(1).result ).map(_.headOption)

  def insert(store: Store): Future[Store] =  db.run( ( ( stores returning stores.map(_.getFields) ) += store ).map(fields => (Store.apply _).tupled(fields)) )

}
