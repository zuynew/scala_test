package dao

import models.{Order, OrderTable}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}

class OrderDAO(db: Database)(implicit ec: ExecutionContext) {

  private val orders = TableQuery[OrderTable]

  def findOpenOrderByUserId(userId: Int): Future[Option[Order]] = {

    db.run(
      orders.filter(order => order.userId === userId && order.status === "open" ).take(1).result
    ).map(_.headOption)

  }

  def all(): Future[Seq[Order]] = db.run(orders.result)

  def insert(order: Order): Future[Order] =  db.run( ( ( orders returning orders.map(_.getFields)) += order ).map(fields => (Order.apply _).tupled(fields)) )

}
