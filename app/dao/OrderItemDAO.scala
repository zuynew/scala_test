package dao

import models.{OrderItem, OrderItemTable}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}


class OrderItemDAO(db: Database, orderDAO: OrderDAO, productDAO: ProductDAO)(implicit ec: ExecutionContext)  {

  private val orderItems = TableQuery[OrderItemTable]

  def all(): Future[Seq[OrderItem]] = db.run(orderItems.result)

  def findByOrderIdAndProductId(orderId: Int, productId: Int): Future[Option[OrderItem]] = db.run(
    orderItems.filter(orderItem => orderItem.orderId === orderId && orderItem.productId === productId).take(1).result
  ).map(_.headOption)

  def insert(orderItem: OrderItem): Future[OrderItem] =  db.run( ( ( orderItems returning orderItems.map(_.getFields) ) += orderItem ).map(fields => (OrderItem.apply _).tupled(fields)) )

  def insertOrUpdate(orderItem: OrderItem): Future[OrderItem] = {
    db.run((for {
      existedOrderItemOption <- orderItems.filter( v => v.orderId === orderItem.orderId && v.productId === orderItem.productId ).result.headOption
      newOrderItemResult <- existedOrderItemOption match {
        case None =>
          ((orderItems returning orderItems.map(_.getFields)) += orderItem).map(
            fields => DBIO.successful((OrderItem.apply _).tupled(fields))
          )
        case Some(existedOrderItem) =>
          orderItems.filter(_.id === existedOrderItem.id).map(_.quantity).update(orderItem.quantity)
            .map(_ => orderItems.filter(_.id === existedOrderItem.id).result.head)
      }
      newOrderItem <- newOrderItemResult
    } yield newOrderItem).transactionally)
  }



}
