package dao

import models.{UsersTable, User}
import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}


class UserDAO(db: Database)(implicit ec: ExecutionContext) {

  private val users = TableQuery[UsersTable]

  def all(): Future[Seq[User]] = db.run(users.result)

  def findByLogin(login: String): Future[Option[User]] = db.run( users.filter(_.login === login).take(1).result ).map(_.headOption)

  def insert(user: User): Future[User] =  db.run( ( ( users returning users.map(_.getFields) ) += user ).map(fields => (User.apply _).tupled(fields)) )

}
