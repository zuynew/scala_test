package exceptions.order

class OpenOrderExistsException (message: Option[String] = Some("Store not found"), cause: Option[Throwable] = None) extends Exception

object OpenOrderExistsException {

  def apply() = new OpenOrderExistsException()

}
