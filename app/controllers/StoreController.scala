package controllers

import exceptions.store.StoreNotFoundException
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import services.StoreService

import scala.concurrent.{ExecutionContext, Future}

class StoreController(storeService: StoreService)(implicit ec: ExecutionContext) extends Controller {

  def view(storeId: Int) = Action.async { request =>
    storeService.getByIdWithProducts(storeId).map({
      case Left(e) => e match {
        case _: StoreNotFoundException => NotFound
      }
      case Right(storeWithProducts) => Ok(Json.toJson(storeWithProducts))
    })
  }

}
