package controllers

import exceptions.order.OpenOrderExistsException
import models.{Order, User}
import play.api.mvc.{Action, Controller}
import services.OrderService

import scala.concurrent.ExecutionContext

class OrderController(orderService: OrderService)(implicit ec: ExecutionContext) extends Controller {

  def create = Action.async { request =>
    // TODO брать юзера из реквеста
    val user = User(id = Some(1), login = "asd", password = "asd")

    orderService.create(user).map({
      case Right(_) => Created
      case Left(e) => e match {
        case _: OpenOrderExistsException => Conflict
        case _ => InternalServerError
      }
    })
  }

}
