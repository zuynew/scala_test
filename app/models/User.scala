package models

import play.api.libs.json.Json
import slick.driver.PostgresDriver.api._

case class User(id:Option[Int] = None, login:String, password:String)

object User {
  implicit val format = Json.format[User]
}

class UsersTable(tag: Tag) extends Table[User](tag, "tbl_user") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def login = column[String]("login")
  def password = column[String]("password")

  def getFields = (id.?, login, password)

  def * = getFields <> ((User.apply _).tupled, User.unapply)
}

