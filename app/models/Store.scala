package models

import play.api.libs.json.Json
import slick.driver.PostgresDriver.api._

case class Store(id:Option[Int] = None, name:String)

object Store {
  implicit val format = Json.format[Store]
}

case class StoreWithProducts(store: Store, products: Seq[Product] = Seq())

object StoreWithProducts {
  implicit val format = Json.format[StoreWithProducts]
}


class StoreTable(tag: Tag) extends Table[Store](tag, "tbl_store") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")

  def getFields = (id.?, name)

  def * = (id.?, name) <> ((Store.apply _).tupled, Store.unapply)
}