name := "scala_test"

version := "1.0"

lazy val `scala_test` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

libraryDependencies ++= Seq(jdbc, evolutions)

//libraryDependencies += "com.typesafe.play" %% "play-slick" % "1.1.1"

libraryDependencies += "com.typesafe.slick" %% "slick" % "3.1.1"

libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1206-jdbc42"

libraryDependencies += "com.softwaremill.macwire" %% "macros" % "2.2.0" % "provided"

libraryDependencies += "com.softwaremill.macwire" %% "util" % "2.2.0"

//libraryDependencies += "com.softwaremill.macwire" %% "proxy" % "2.2.0"

libraryDependencies += "org.mindrot" % "jbcrypt" % "0.3m"

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")

routesGenerator := InjectedRoutesGenerator